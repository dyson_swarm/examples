﻿using DIExamples.Core.Abstractions;
using DIExamples.Core.Factories;
using Microsoft.Extensions.DependencyInjection;
using Pathfinder1.Core.Abstractions;
using Pathfinder1.Core.BaseInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DIExamples.Core.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddPathfinder1Services(this IServiceCollection serviceCollection)
        {
            AddAllPCBuilderClasses(serviceCollection);
            serviceCollection.AddTransient<INameBuilder, NameBuilder>();
            return serviceCollection;
        }

        private static void AddAllPCBuilderClasses(IServiceCollection sc)
        {
            var t = Assembly
                            .GetEntryAssembly()
                            .GetReferencedAssemblies().ToList();
            var classes = Assembly
                            .GetEntryAssembly()
                            .GetReferencedAssemblies()
                            .Select(Assembly.Load)
                            .SelectMany(x => x.DefinedTypes)
                            .Where(x => !x.IsAbstract)
                            .Where(type => typeof(IPCBuilder).IsAssignableFrom(type))
                            .ToList();

            foreach (var c in classes)
            {
                sc.AddTransient(c.AsType());
            }
        }
    }
}
