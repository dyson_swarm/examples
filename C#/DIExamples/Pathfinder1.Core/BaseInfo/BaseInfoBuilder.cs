﻿using DIExamples.Core.Abstractions;
using Microsoft.AspNetCore.Http;
using Pathfinder1.Core.Abstractions;
using System;
using System.Threading.Tasks;

namespace Pathfinder1.Core.BaseInfo
{
    public class BaseInfoBuilder : IPCBuilder
    {
        private readonly IPCBuilderFactory _factory;
        public BaseInfoBuilder(INameBuilder nameBuilder, IPCBuilderFactory factory, IHttpContextAccessor acc)
        {
            _factory = factory;
        }

        public Task<IPart> BuildPartAsync()
        {
            throw new NotImplementedException();
        }
    }
}
