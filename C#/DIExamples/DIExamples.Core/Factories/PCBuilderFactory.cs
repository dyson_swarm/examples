﻿using DIExamples.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIExamples.Core.Factories
{
    public class PCBuilderFactory : IPCBuilderFactory
    {
        private readonly Dictionary<string, Type> _typeLookup = new Dictionary<string, Type>();
        private readonly IServiceProvider _serviceProvider;
        public PCBuilderFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public IPCBuilder GetPCBuilder(string typeName)
        {
            Type type;
            if (_typeLookup.ContainsKey(typeName))
            {
                type = _typeLookup[typeName];
            }
            else
            {
                type = Type.GetType(typeName);
                _typeLookup[typeName] = type;
            }

            return (IPCBuilder)_serviceProvider.GetService(type);
        }
    }
}
