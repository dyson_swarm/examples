﻿using DIExamples.Core.Abstractions;
using DIExamples.Core.Factories;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DIExamples.Core.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddPCBuilderServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton<IPCBuilderFactory, PCBuilderFactory>();
            return serviceCollection;
        }
    }
}
