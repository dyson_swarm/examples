﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DIExamples.Core.Abstractions
{
    /// <summary>
    /// Interface to define contruction method for build a Pathfinder PC
    /// </summary>
    public interface IPCBuilder
    {
       Task<IPart> BuildPartAsync();
    }
}
