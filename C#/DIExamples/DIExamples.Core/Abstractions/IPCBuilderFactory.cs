﻿using DIExamples.Core.Abstractions;

namespace DIExamples.Core.Abstractions
{
    public interface IPCBuilderFactory
    {
        IPCBuilder GetPCBuilder(string typeName);
    }
}